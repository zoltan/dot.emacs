;;; python-venv-tracker.el -- Tracks and activates the correct virtualenv

;;; Commentary:

;;; This mode relies on two hooks:
;;;
;;; - `post-command-hook': try to set the current venv to the value of
;;;   `zk/py-buffer-venv' if it is not already active
;;; - `python-mode-hook': every time a python buffer is activated, try to find
;;;   a virtualenv for that buffer, stores it to `zk/py-buffer-venv'  and activates
;;;   it.

;;; Code:

(require 'pyvenv)


(defvar zk/py-buffer-venv nil
  "Path of the python virtualenv of the buffer.")

(make-variable-buffer-local 'zk/py-buffer-venv)

(defmacro zk/with-no-venv (body)
  "Execute BODY without a virtualenv active and restore the venv after."
  `(let ((saved-venv pyvenv-virtual-env))
     (pyvenv-deactivate)
     (let ((retval ,body))
       (when saved-venv
         (pyvenv-activate saved-venv))
       retval)))

(defun zk/venv-manager-in-dir-p (dir)
  "Tell if DIR has a file that defines a python virtualenv."
  (directory-files dir nil "^Pipfile$\\|^pyproject\\.toml$\\|^\\.?venv$" nil))

(defun zk/py-get-venv-path ()
  "Get the path of the virtualenv for the current file."
  (let* ((venv-dir (locate-dominating-file
                    default-directory
                    #'zk/venv-manager-in-dir-p))
         (venv-file (when venv-dir
                      (car (zk/venv-manager-in-dir-p venv-dir)))))
    (when venv-file
      (pcase venv-file
        (".venv" (format "%s/.venv" venv-dir))
        ("venv" (format "%s/venv" venv-dir))
        ("pyproject.toml"
         (if (file-directory-p (format "%s/.venv" venv-dir ))
             (format "%s/.venv" venv-dir )
           (substring (shell-command-to-string "poetry env info --path") 0 -1)))
        ("Pipfile" (shell-command-to-string "pipenv --venv"))
        (_ (error "Unknown file: %S" venv-file))))))

(defun zk/py-set-venv ()
  "Activate a virtualenv for the current buffer."
  (interactive)
  (let ((venv-path (zk/with-no-venv (zk/py-get-venv-path))))
    (when venv-path
      (pyvenv-activate venv-path)
      (setq zk/py-buffer-venv pyvenv-virtual-env)
      (message "Venv in %s activated." zk/py-buffer-venv))))

;;;###autoload
(define-minor-mode zk/py-venv-tracker-mode
  "Global minor mode to activate the correct python virtualenv."
  :global t
  (if zk/py-venv-tracker-mode
      (progn
        (add-hook 'post-command-hook #'zk/py-venv-tracker)
        (add-hook 'python-mode-hook #'zk/py-set-venv -10)
        (add-hook 'python-ts-mode-hook #'zk/py-set-venv -10))
    (progn
      (remove-hook 'post-command-hook #'zk/py-venv-tracker)
      (remove-hook 'python-mode-hook #'zk/py-set-venv)
      (remove-hook 'python-ts-mode-hook #'zk/py-set-venv))))

(defun zk/py-venv-tracker ()
  "Activate the correct venv for the current file."
  (when (and zk/py-buffer-venv
             (not (string= zk/py-buffer-venv pyvenv-virtual-env)))
    (pyvenv-activate zk/py-buffer-venv)
    (message "Venv in %s activated." zk/py-buffer-venv)))

(provide 'python-venv-tracker)

;;; python-venv-tracker.el ends here
